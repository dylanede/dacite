# dacite-winit Changelog

## Version 0.3.0, released on 06.06.2017

 - Compatibility with dacite 0.3.0


## Version 0.2.0, released on 05.06.2017

 - Compatibility with dacite 0.2.0


## Version 0.1.0, released on 05.06.2017

 - This is the initial release of dacite-winit.
